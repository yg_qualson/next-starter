import { useEffect, useRef } from 'react'

class Timer {
  private timerId: number
  private remaining: number
  private startTime: number
  private callback: () => void

  constructor(callback: () => void, delay: number) {
    this.startTime = Date.now()
    this.remaining = delay
    this.callback = callback
    this.timerId = this.start()
  }

  private start() {
    return window.setTimeout(this.callback, this.remaining)
  }

  pause() {
    clearTimeout(this.timerId)
    this.remaining -= Date.now() - this.startTime
  }

  resume() {
    clearTimeout(this.timerId)
    this.startTime = Date.now()
    this.timerId = this.start()
  }

  clear() {
    clearTimeout(this.timerId)
  }
}

export function useTimer(callback: () => void, timeout: number) {
  const timer = useRef<Timer | null>(null)
  useEffect(() => {
    startTimer()
    return () => clearTimer()
  }, [])

  const startTimer = () => {
    timer.current = new Timer(callback, timeout)
  }

  const clearTimer = () => {
    if (timer.current) timer.current.clear()
  }
}
