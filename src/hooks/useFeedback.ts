import { v4 as uuidv4 } from 'uuid'

import modalActions from '@store/modal/modal.actions'
import notificationActions from '@store/notification/notification.actions'
import { NotificationType } from '@store/notification/notification.types'
import { useDispatch } from '@store/useStore'

import { AlertProps } from '@modals/Alert'
import { ConfirmProps } from '@modals/Confirm'

export default function useFeedback() {
  const dispatch = useDispatch()

  function notify(content: React.ReactNode, type = NotificationType.INFO) {
    const id = uuidv4()
    dispatch(notificationActions.notify({ id, type, content }))
    return id
  }

  function dismiss(id: string) {
    dispatch(notificationActions.dismiss({ id }))
  }

  function alert(props: AlertProps) {
    dispatch(modalActions.openModal({ type: 'Alert', props }))
  }

  function confirm(props: ConfirmProps) {
    dispatch(modalActions.openModal({ type: 'Confirm', props }))
  }

  function closeAll() {
    dispatch(modalActions.closeAll())
  }

  return { notify, dismiss, alert, confirm, closeAll }
}
