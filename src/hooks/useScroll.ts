import { useState, useEffect, useRef } from 'react'

import { throttle } from '@utils/event'

export const useScrollTop = () => {
  const [scrollTop, setScrollTop] = useState(0)

  const updateScroll = throttle((e: Event) => {
    // update scrollTop
    const newScrollTop = window.pageYOffset
    setScrollTop(newScrollTop)
  }, 100)

  // Scroll detect
  useEffect(() => {
    window.addEventListener('scroll', updateScroll)
    return () => {
      window.removeEventListener('scroll', updateScroll)
    }
  }, [])
  return scrollTop
}

export const useHideOnScrollDown = ({ minimumScroll = 200 }) => {
  const prevScrollTop = useRef(0)
  const [hide, setHide] = useState(false)
  const scrollTop = useScrollTop()

  // Set Hide or not
  useEffect(() => {
    // decide hide or show
    if (scrollTop < minimumScroll) return setHide(false)
    // show when user scroll upward
    setHide(prevScrollTop.current <= scrollTop)
    // save as prev-value
    prevScrollTop.current = scrollTop
  }, [scrollTop])

  return hide
}
