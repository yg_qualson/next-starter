import Image from 'next/image'
import styled from 'styled-components'

interface Props {
  src: string
  width?: number | string
  height?: number | string
  objectFit?: React.CSSProperties['objectFit']
  objectPosition?: React.CSSProperties['objectPosition']
  style?: React.CSSProperties
}
const BackgroundImage: React.FC<Props> = ({ children, style, ...imageProps }) => {
  // set default value for imageProps
  const { width, height, src, objectFit = 'cover', objectPosition = 'center' } = imageProps
  return (
    <Background style={{ width, height, ...style }}>
      <Image src={src} layout="fill" objectFit={objectFit} objectPosition={objectPosition} />
      {children && <Foreground>{children}</Foreground>}
    </Background>
  )
}

export default BackgroundImage

const Background = styled.div`
  position: relative;
`
const Foreground = styled.div`
  position: relative;
  z-index: 1;
`
