import { TransitionStatus } from 'react-transition-group/Transition'

import { NotificationType } from '@store/notification/notification.types'

import { useTimer } from '@hooks/useTimer'

import * as S from './Notification.style'

interface Props {
  children: React.ReactNode
  type: NotificationType
  transitionState: TransitionStatus
  autoDismissTimeout: number
  onDismiss(): void
}
const Notification: React.FC<Props> = ({ type, transitionState, autoDismissTimeout, onDismiss, children }) => {
  useTimer(onDismiss, autoDismissTimeout)

  return (
    <S.Notification type={type} status={transitionState} onClick={onDismiss}>
      {children}
    </S.Notification>
  )
}

export default Notification
