import { TransitionStatus } from 'react-transition-group/Transition'
import styled, { css, SimpleInterpolation } from 'styled-components'

import { NotificationType } from '@store/notification/notification.types'

export const transitionDuration = 250
export const autoDismissTimeout = 3333

export const Container = styled.div`
  position: fixed;
  box-sizing: border-box;
  max-height: 100%;
  overflow: hidden;
  z-index: 1000;
`

const notificationStatus: Record<TransitionStatus, SimpleInterpolation> = {
  entering: css`
    transform: translate3d(120%, 0, 0);
  `,
  entered: css`
    transform: translate3d(0, 0, 0);
  `,
  exiting: css`
    transform: scale(1, 0.66);
    transform-origin: top;
    opacity: 0;
  `,
  exited: css`
    transform: scale(1, 0.66);
    transform-origin: top;
    opacity: 0;
  `,
  unmounted: css`
    transform: scale(1, 0.66);
    opacity: 0;
  `,
}

const notificationType: Record<NotificationType, SimpleInterpolation> = {
  [NotificationType.INFO]: css`
    background-color: black;
    color: white;
  `,
  [NotificationType.WARN]: css`
    background-color: hsl(50, 100%, 60%);
    color: black;
  `,
  [NotificationType.ERROR]: css`
    background-color: hsl(10, 80%, 50%);
    color: white;
  `,
}

export const Notification = styled.div<{ status: TransitionStatus; type: NotificationType }>`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  width: 100%;
  font-size: 12px;
  height: 40px;
  transition: transform ${transitionDuration}ms cubic-bezier(0.2, 0, 0, 1), opacity ${transitionDuration}ms;

  display: flex;
  justify-content: center;
  align-items: center;

  ${({ status }) => notificationStatus[status]};
  ${({ type }) => notificationType[type]};
`
