import { TransitionGroup, Transition } from 'react-transition-group'

import notificationActions from '@store/notification/notification.actions'
import { useDispatch, useSelector } from '@store/useStore'

import Notification from './Notification'
import * as S from './Notification.style'

const NotificationContainer = () => {
  const dispatch = useDispatch()
  const notifications = useSelector((state) => state.notification)
  function dismiss(id: string) {
    dispatch(notificationActions.dismiss({ id }))
  }

  return (
    <S.Container>
      <TransitionGroup>
        {notifications.map(({ type, id, content }) => (
          <Transition mountOnEnter unmountOnExit key={id} timeout={S.transitionDuration}>
            {(transitionState) => (
              <Notification
                type={type}
                key={id}
                onDismiss={() => dismiss(id)}
                transitionState={transitionState}
                autoDismissTimeout={S.autoDismissTimeout}
              >
                {content}
              </Notification>
            )}
          </Transition>
        ))}
      </TransitionGroup>
    </S.Container>
  )
}

export default NotificationContainer
