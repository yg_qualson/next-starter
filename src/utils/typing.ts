// type utility
export function removeNulls<S>(value: S | undefined): value is S {
  return Boolean(value)
}
