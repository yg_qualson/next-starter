export const debounce = <F extends (...args: any) => any>(func: F, waitFor: number = 300) => {
  let timer: number | null = null
  const debounced = (...args: any) => {
    if (timer) clearTimeout(timer)
    timer = window.setTimeout(() => func(...args), waitFor)
  }

  return debounced as (...args: Parameters<F>) => ReturnType<F>
}

export function throttle<Params extends any[]>(func: (...args: Params) => void, timeout: number) {
  let timer: number | null = null
  return (...args: Params) => {
    if (timer) return
    timer = window.setTimeout(() => {
      func(...args)
      timer = null
    }, timeout)
  }
}
