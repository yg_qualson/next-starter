import { createContext, useEffect, useState } from 'react'
import { useMediaQuery } from 'react-responsive'
import styled, { ThemeProvider } from 'styled-components'

import { MIMINUM, MOBILE_MAX, TABLETP_MAX, TABLETL_MAX } from '@styles/breakpoints'

const ResponsiveContext = createContext({ desktop: false, tabletL: false, tabletP: false, mobile: true })

// fix responsive + hydration issue
const HideAndLoad = styled.div<{ isInitialRender: boolean }>`
  visibility: ${({ isInitialRender }) => (isInitialRender ? 'hidden' : 'visible')};
`

function useResponsive() {
  const [isMounted, setMounted] = useState(false)
  useEffect(() => setMounted(true), [])

  const isDesktop = useMediaQuery({ minWidth: TABLETL_MAX }) && isMounted
  const isTabletLandsacpe = useMediaQuery({ minWidth: TABLETP_MAX, maxWidth: TABLETL_MAX - 1 }) && isMounted
  const isTabletPortrait = useMediaQuery({ minWidth: MOBILE_MAX, maxWidth: TABLETP_MAX - 1 }) && isMounted
  const isMobile = useMediaQuery({ minWidth: MIMINUM, maxWidth: MOBILE_MAX - 1 }) || !isMounted

  const context = { desktop: isDesktop, tabletL: isTabletLandsacpe, tabletP: isTabletPortrait, mobile: isMobile }
  return { isMounted, ...context }
}

export const withResponsive = <P,>(Component: React.ComponentType<P>) => (props: P) => {
  const { isMounted, ...context } = useResponsive()

  return (
    <ThemeProvider theme={context}>
      <ResponsiveContext.Provider value={context}>
        <HideAndLoad isInitialRender={!isMounted}>
          <Component {...props} />
        </HideAndLoad>
      </ResponsiveContext.Provider>
    </ThemeProvider>
  )
}
