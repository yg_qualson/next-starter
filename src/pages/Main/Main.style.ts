import styled from 'styled-components'

export const Title = styled.h1`
  font-size: 2em;
  color: hsl(210, 100%, 50%);
`
export const ModalTester = styled.button`
  padding: 5px;
  border-radius: 3px;
  background: grey;
  color: white;
`

export const BottomFloatingBox = styled.div`
  width: 100%;
  height: 50px;
  background: transparent;
`
export const BottomFloating = styled.div`
  position: fixed;
  bottom: 0;
  width: 100%;
  height: 50px;
  background: greenyellow;
`
