import Image from 'next/image'

import Layout from '@pages/Layout'

import BackgroundImage from '@components/BackgroundImage/BackgroundImage'
import useFeedback from '@hooks/useFeedback'

import { ContentsWidthSection, FullWidthSection } from '@styles/wrappers'

import * as S from './Main.style'

const Main = () => {
  const { notify, alert } = useFeedback()

  function openAlert() {
    alert({ message: 'Hey!' })
  }

  function notificationTest() {
    notify('Test')
  }

  return (
    <Layout>
      <ContentsWidthSection>
        <S.Title>Main page</S.Title>
        <S.ModalTester onClick={openAlert}>Hey</S.ModalTester>
        <S.ModalTester onClick={notificationTest}>NOTI</S.ModalTester>
        <BackgroundImage
          src="https://public.realclass.co.kr/images/promotion/package/scholarship_top_bg@2x.png"
          width="100%"
          style={{ paddingBottom: `${(690 / 1300) * 100}%`, margin: '0 auto' }}
        >
          <span style={{ color: 'white' }}>TEST</span>
        </BackgroundImage>
      </ContentsWidthSection>
      <FullWidthSection style={{ height: 500, background: 'black' }}>
        <Image
          src="https://public.realclass.co.kr/images/promotion/package/scholarship_top_bg@2x.png"
          layout="fill"
          objectFit="contain"
        />
      </FullWidthSection>
      <ContentsWidthSection>
        <div
          style={{
            width: '100%',
            height: 'calc(100vh + 200px)',
            background: 'linear-gradient(to bottom, red -20%, yellow,  blue 120%)',
          }}
        >
          스크롤 테스트용
        </div>
      </ContentsWidthSection>
      {/* 하단 fixed 컴포넌트는 아래와 같은 방식을 사용하자 */}
      <S.BottomFloatingBox />
      <S.BottomFloating />
    </Layout>
  )
}

export default Main
