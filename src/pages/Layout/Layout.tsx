import * as S from './Layout.style'
import Header from './components/Header'

interface Props {
  header?: { hide?: boolean; sticky?: boolean }
}
const Layout: React.FC<Props> = ({ header, children }) => {
  const { hide: hideHeader = false, sticky: stickyHeader = true } = header || {}

  return (
    <S.Layout>
      {!hideHeader && <Header sticky={stickyHeader} />}
      <S.Main>{children}</S.Main>
    </S.Layout>
  )
}

export default Layout
