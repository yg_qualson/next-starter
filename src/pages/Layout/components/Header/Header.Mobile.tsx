import { useHideOnScrollDown } from '@hooks/useScroll'

import * as S from './Header.style'

interface Props {
  sticky?: boolean
}
const Header: React.FC<Props> = ({ sticky = true }) => {
  const hide = useHideOnScrollDown({ minimumScroll: 200 })

  return <S.HeaderMobile sticky={sticky} hide={hide}></S.HeaderMobile>
}

export default Header
