import { useContext } from 'react'

import LayoutContext from '@pages/Layout/context'

import HeaderDesktop from './Header.Desktop'
import HeaderMobile from './Header.Mobile'

interface Props {
  sticky?: boolean
}
const Header: React.FC<Props> = ({ sticky }) => {
  const { desktop } = useContext(LayoutContext)
  if (desktop) {
    return <HeaderDesktop sticky={sticky} />
  }
  return <HeaderMobile sticky={sticky} />
}

export default Header
