import styled, { css } from 'styled-components'

const stickyTop = css`
  position: sticky;
  top: 0;
`
const hidden = css`
  transform: translateY(-100%);
`

export const Header = styled.header<{ sticky?: boolean; hide: boolean }>`
  position: relative;
  width: 100%;
  background: #bbb;
  z-index: 101;
  transform: translateY(0);
  transition: transform 0.5s;

  ${({ sticky }) => sticky && stickyTop}
  ${({ hide }) => hide && hidden}
`

export const HeaderDesktop = styled(Header)`
  height: 60px;
  padding: 0 50px;
`

export const HeaderMobile = styled(Header)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 54px;
  padding: 0 15px;
`
