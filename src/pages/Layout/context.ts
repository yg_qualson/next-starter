import { createContext } from 'react'

// default as mobile
const LayoutContext = createContext({ desktop: false, tablet: false, mobile: true })

export default LayoutContext
