import styled from 'styled-components'

import * as CS from '../common.style'

export const ConfirmButton = styled(CS.SmallModalButton)`
  width: 50%;
  border-left: 1px solid #bbb;
`
export const CancelButton = styled(CS.SmallModalButton)`
  width: 50%;
  color: #333;
`

export const Confirm = styled(CS.SmallModal)`
  /* customization */
`
