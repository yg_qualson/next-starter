import { confirm, cancel } from '@store/modal/modal.actions'
import { useDispatch } from '@store/useStore'

import * as CS from '../common.style'
import { Modal } from '../types'
import * as S from './styled'

export interface ConfirmProps {
  title?: string
  message: string | React.ReactNode
  confirmText?: string
  cancelText?: string
  onConfirm?: () => void
  onCancel?: () => void
}

const Confirm: Modal<ConfirmProps> = ({
  title,
  message,
  confirmText = '확인',
  cancelText = '취소',
  onConfirm,
  onCancel,
  close,
}) => {
  const dispatch = useDispatch()
  function handleConfirm() {
    dispatch(confirm())
    onConfirm?.()
    close()
  }
  function handleCancel() {
    dispatch(cancel())
    onCancel?.()
    close()
  }
  return (
    <S.Confirm>
      <CS.Body>
        {title && <CS.Title>{title}</CS.Title>}
        <CS.Message noTitle={!title}>{message}</CS.Message>
      </CS.Body>
      <CS.Buttons>
        <S.CancelButton onClick={handleCancel}>{cancelText}</S.CancelButton>
        <S.ConfirmButton onClick={handleConfirm}>{confirmText}</S.ConfirmButton>
      </CS.Buttons>
    </S.Confirm>
  )
}

export default Confirm
