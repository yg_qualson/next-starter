import * as CS from '../common.style'
import { Modal } from '../types'
import * as S from './styled'

export interface AlertProps {
  title?: string
  message: string | React.ReactNode
  confirmText?: string
  onClose?: () => void
}

const Alert: Modal<AlertProps> = ({ title, message, confirmText = '확인', onClose, close }) => {
  function handleConfirm() {
    onClose?.()
    close()
  }
  return (
    <S.Alert>
      <CS.Body>
        {title && <CS.Title>{title}</CS.Title>}
        <CS.Message noTitle={!title}>{message}</CS.Message>
      </CS.Body>
      <CS.Buttons>
        <S.ConfirmButton onClick={handleConfirm}>{confirmText}</S.ConfirmButton>
      </CS.Buttons>
    </S.Alert>
  )
}

export default Alert
