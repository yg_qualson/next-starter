import styled from 'styled-components'

import * as CS from '../common.style'

export const ConfirmButton = styled(CS.SmallModalButton)`
  width: 100%;
`

export const Alert = styled(CS.SmallModal)`
  /* customization */
`
