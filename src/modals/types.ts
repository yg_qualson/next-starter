import modalRegister from './register'

export type Modal<P> = React.FC<P & BasicModalProps>
export type Modals = typeof modalRegister
export type ModalType = keyof Modals

export interface BasicModalProps {
  close: () => void
}
export type ModalProps<T extends ModalType> = Omit<React.ComponentProps<Modals[T]>, keyof BasicModalProps>
