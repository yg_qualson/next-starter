import styled, { css } from 'styled-components'

const dimmedBackground = css`
  background: rgba(0, 0, 0, 0.6);
`

export const Overlay = styled.div<{ dim: boolean; visible: boolean }>`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 1000;
  ${({ dim }) => dim && dimmedBackground}

  // animation
  opacity: 0;
  pointer-events: none;
  transition: opacity 0.2s;
  ${({ visible }) =>
    visible &&
    css`
      opacity: 1;
      pointer-events: initial;
    `}
`
