import { useRouter } from 'next/router'
import { useEffect, useState, cloneElement } from 'react'

import modalActions from '@store/modal/modal.actions'
import { OverlayOptions } from '@store/modal/modal.types'
import { useDispatch, useSelector } from '@store/useStore'

import modalRegister from '../register'
import * as S from './styled'

const ModalContainer = () => {
  const dispatch = useDispatch()
  const router = useRouter()

  // 뒤로가기 시 모달 닫기
  useEffect(() => {
    router.beforePopState(() => {
      dispatch(modalActions.closeAll())
      return true
    })
  }, [])

  const openedModals = useSelector((state) => state.modal)

  return (
    <div id="modal-root">
      {openedModals.map(({ type, props, overlayOptions }, index) => {
        const ModalComponent = modalRegister[type]
        const close = () => dispatch(modalActions.closeModal({ type }))
        return (
          <ModalOverlay key={index} {...overlayOptions} closeSelf={close}>
            <ModalComponent {...(props as any)} />
          </ModalOverlay>
        )
      })}
    </div>
  )
}

interface OverlayProps extends OverlayOptions {
  closeSelf: () => void
  children: React.ReactElement
}
const ModalOverlay: React.FC<OverlayProps> = ({
  dim = true,
  closeOnOverlayClick = true,
  preventScroll = true,
  children,
  closeSelf,
}) => {
  // animated close
  const [visible, setVisible] = useState(false)
  useEffect(() => setVisible(true), [])

  function delayedClose() {
    setVisible(false)
    setTimeout(closeSelf, 200)
  }

  const onClick = (e: React.MouseEvent) => {
    if (!closeOnOverlayClick) return
    if (e.target === e.currentTarget) {
      delayedClose()
    }
  }

  useEffect(() => {
    if (preventScroll) {
      document.body.style.overflow = 'hidden'
      return () => {
        document.body.style.overflow = 'initial'
      }
    }
  }, [])

  return (
    <S.Overlay dim={dim} visible={visible} onClick={onClick}>
      {cloneElement(children, { close: delayedClose })}
    </S.Overlay>
  )
}
export default ModalContainer
