import styled, { css, keyframes } from 'styled-components'

/*** COMMON ***/
export const Title = styled.h4`
  width: 100%;
  margin: 0;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  color: #000;
`

export const Body = styled.div`
  position: relative;
  padding: 0 30px;
  text-align: center;
  overflow-y: auto;
`

export const Buttons = styled.div`
  width: 100%;
`

export const Message = styled.p<{ noTitle: boolean }>`
  padding-bottom: 30px;
  font-size: 14px;
  line-height: 1.5;
  color: #333;
  white-space: pre-line;
  ${({ noTitle }) =>
    noTitle &&
    css`
      font-size: 16px;
      color: #000;
    `}
`

/*** SmallModal ***/
export const SmallModalButton = styled.button`
  height: 48px;
  padding: 0 20px;
  background-color: #ccc;
  color: #000;
  font-size: 16px;
  font-weight: 500;

  // border-radius + overflow:hidden safari 버그로 추가(모서리 영역에서 clipping이 되지 않음)
  transform: translateZ(0);

  &:active {
    background-color: #ddd;
  }
  &:first-child {
    border-bottom-left-radius: 16px;
  }
  &:last-child {
    border-bottom-right-radius: 16px;
  }
`

export const SmallModal = styled.div`
  position: relative;
  width: 320px;
  border-radius: 16px;
  background: white;
  overflow: hidden;
  ${Body} {
    height: calc(100% - 48px);
    padding-top: 30px;
  }
  ${Title} {
    padding-bottom: 30px;
    line-height: 1.5;
  }
  ${Buttons} {
    position: sticky;
    bottom: 0;
  }
`
