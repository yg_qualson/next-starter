import { createGlobalStyle } from 'styled-components'

import { baseFont } from './font'

const BaseStyle = createGlobalStyle`
  :root {
    --vh: calc(100vh - env(safe-area-inset-top, 0) - env(safe-area-inset-bottom, 0));
  }

  html {
    font: normal 16px ${baseFont}; 
    -webkit-font-smoothing: antialiased;
    touch-action: manipulation;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    scroll-behavior: smooth;
    -ms-overflow-style: scrollbar;
  }

  body {
    line-height: 1.5;
    overflow-wrap: break-word;
    overflow-y: scroll;
    -ms-overflow-style: scrollbar;
  }
`

export default BaseStyle
