import styled, { css } from 'styled-components'

import { MIMINUM } from './breakpoints'

export const FullWidthSection = styled.section`
  position: relative;
  width: 100%;
`

export const ContentsWidthSection = styled.section`
  position: relative;
  ${({ theme }) =>
    theme.mobile &&
    css`
      width: 87.5vw;
      margin: 0 auto;
    `};
  ${({ theme }) =>
    theme.tabletP &&
    css`
      width: 600px;
      margin: 0 auto;
    `};
  ${({ theme }) =>
    theme.tabletL &&
    css`
      width: 900px;
      margin: 0 auto;
    `};

  ${({ theme }) =>
    theme.desktop &&
    css`
      width: 1200px;
      margin: 0 auto;
    `};

  @media (max-width: ${MIMINUM}px) {
    width: 100%;
  }
`
