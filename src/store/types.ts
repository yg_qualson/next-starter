import { AxiosResponse } from 'axios'
import { ActionType, PayloadActionCreator } from 'typesafe-actions'

import rootAction from './actions'
import rootReducer from './reducers'

export type RootAction = ActionType<typeof rootAction>
export type RootState = ReturnType<typeof rootReducer>

interface Callbacks<Result> {
  onSuccess?: (result: Result) => void
  onFailure?: (error: unknown) => void
  // on success or finish, whatever
  onFinish?: () => void
}
export type WithCallback<Result, RequestPayload = undefined> = RequestPayload extends undefined
  ? Callbacks<Result>
  : RequestPayload & Callbacks<Result>

export interface Success<Result = unknown, Payload = undefined> {
  request: Payload
  response: Result
}
export interface Failure<Payload = undefined> {
  request: Payload
  error: unknown
}

/* createAPIsSaga */
export type APICall<P, Data> = (payload: P) => Promise<AxiosResponse<Data>>

export interface AsyncAction<P, R> {
  request: PayloadActionCreator<string, WithCallback<R, P>>
  success: PayloadActionCreator<string, Success<R, P>>
  failure: PayloadActionCreator<string, Failure<P>>
}
