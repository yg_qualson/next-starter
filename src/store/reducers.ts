import { combineReducers } from 'redux'

import example from '@store/example/example.reducer'
import modal from '@store/modal/modal.reducer'
import notification from '@store/notification/notification.reducer'

const rootReducer = combineReducers({
  example,
  modal,
  notification,
})

export default rootReducer
