import { AxiosError } from 'axios'
import { useSelector as useSelectorHook, useDispatch as useDispatchHook, TypedUseSelectorHook } from 'react-redux'
import { Dispatch } from 'redux'
import { SelectEffect } from 'redux-saga/effects'
import { put, call } from 'typed-redux-saga'
import { TypeConstant, createAsyncAction as originalCreateAsyncAction } from 'typesafe-actions'

import { RootAction, RootState, Success, Failure, AsyncAction, WithCallback, APICall } from './types'

export const useDispatch = () => useDispatchHook<Dispatch<RootAction>>()
export const useSelector = useSelectorHook as TypedUseSelectorHook<RootState>

// register RootState to typed-redux-saga select effect
declare module 'typed-redux-saga' {
  function select<Fn extends (state: RootState) => TSelected, TSelected>(
    selector: Fn
  ): SagaGenerator<ReturnType<Fn>, SelectEffect>
}

// register RootAction to typesafe-actions
declare module 'typesafe-actions' {
  interface Types {
    RootAction: RootAction
  }
}

/**
 * for easy typing of success and failure payload
 */
export function createAsyncAction<RType extends TypeConstant, SType extends TypeConstant, FType extends TypeConstant>(
  request: RType,
  success: SType,
  failure: FType
) {
  function internal<Payload = undefined, Result = unknown>() {
    return originalCreateAsyncAction(request, success, failure)<
      WithCallback<Result, Payload>,
      Success<Result, Payload>,
      Failure<Payload>
    >()
  }
  return internal
}

export function createAPISaga<P, R>(asyncAction: AsyncAction<P, R>, api: APICall<P, R>) {
  return (action: ReturnType<typeof asyncAction['request']>) => runAPISaga(action.payload, asyncAction, api)
}

export function* runAPISaga<P, R>(payload: WithCallback<R, P>, asyncAction: AsyncAction<P, R>, api: APICall<P, R>) {
  const { onSuccess, onFailure, onFinish, ...requestPayload } = payload
  const purePayload = (requestPayload as unknown) as P
  try {
    const response = yield* call(api, purePayload)
    yield* put(asyncAction.success({ request: purePayload, response: response.data }))
    onSuccess?.(response.data)
  } catch (error) {
    yield* put(asyncAction.failure({ request: purePayload, error }))
    onFailure?.(error)
    yield* errorHandlerSaga(error)
  } finally {
    onFinish?.()
  }
}

export function* errorHandlerSaga(error: unknown) {
  if (isAxiosError(error) && error.response) {
    // invalid token
    if (error.response?.status === 401) {
      console.log('토큰이 만료되었습니다. 다시 로그인해주세요.')
    }
    console.error('Error data: ', error.response.data)
  }
  console.error('Error: ', error)
}

export function isAxiosError(error: unknown): error is AxiosError {
  return (error as AxiosError).isAxiosError
}
