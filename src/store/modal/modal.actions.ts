import { PayloadAction, createAction } from 'typesafe-actions'

import { ModalType } from '@modals/types'

import { OpenModalPayload } from './modal.types'

/**
 * example
 * openModal({
    type: 'Confirm',              // Modal name from register
    props: {                      // <Confirm/>'s props
      title: 'ConfirmTitle',
      message: 'Message Text',
      confirmText: 'Yes',
      cancelText: 'No',
    },
    overlayOptions: { 
      dim: true,                  // default true
      closeOnOverlayClick: false, // default true
    },
    preventAnotherModal: false    // default false
  })
 */

type OpenModal = <T extends ModalType>(
  payload: OpenModalPayload<T>
) => PayloadAction<'@modal/OPEN_MODAL', OpenModalPayload<T>>
export const openModal = createAction('@modal/OPEN_MODAL')() as OpenModal
export const closeModal = createAction('@modal/CLOSE_MODAL')<{ type: ModalType }>()
export const closeAll = createAction('@modal/CLOSE_ALL')()

export const confirm = createAction('@modal/CONFIRM_CONFIRM')()
export const cancel = createAction('@modal/CONFIRM_CANCEL')()

export default {
  openModal,
  closeModal,
  closeAll,
  confirm,
  cancel,
}
