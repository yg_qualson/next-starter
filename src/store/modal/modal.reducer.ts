import { createReducer } from 'typesafe-actions'

import { ModalType } from '@modals/types'

import actions from './modal.actions'
import { OpenModalPayload } from './modal.types'

export type ModalState = OpenModalPayload<ModalType>[]
const initialState: ModalState = []

export default createReducer(initialState)
  .handleAction(actions.openModal, (state, action) => {
    const { preventAnotherModal } = action.payload

    // 새로 열리는 모달이 다른 모달을 prevent할 때
    if (preventAnotherModal) return [action.payload]

    // 이미 열려있는 모달이 다른 모달을 prevent할 때
    if (state.some((modal) => modal.preventAnotherModal)) return state

    return [...state, action.payload]
  })
  .handleAction(actions.closeModal, (state, action) => {
    return state.filter((modal) => modal.type !== action.payload.type)
  })
  .handleAction(actions.closeAll, () => initialState)
