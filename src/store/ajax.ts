import axios, { AxiosRequestConfig } from 'axios'
import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig()
const API_ENDPOINT = publicRuntimeConfig.API_ENDPOINT || '/api'

export const ACCESS_TOKEN = '@project/access_token'
export const REFRESH_TOKEN = '@project/refresh_token'
export const EXPIRES = '@project/expires'

export const INTERNAL_API = axios.create({
  baseURL: '/api',
  timeout: 10000,
  withCredentials: true,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
})

export const API = axios.create({
  baseURL: `${API_ENDPOINT}/api`,
  timeout: 10000,
  withCredentials: true,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
})

function useToken(config: AxiosRequestConfig): AxiosRequestConfig {
  const accessToken = localStorage.getItem(ACCESS_TOKEN)

  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  config.headers['Authorization'] = `Bearer ${accessToken || ''}`
  return config
}

API.interceptors.request.use(useToken)
