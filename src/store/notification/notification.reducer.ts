import { createReducer } from 'typesafe-actions'

import actions from './notification.actions'
import * as T from './notification.types'

type State = T.NotifyPayload[]
const initialState: State = []

export default createReducer(initialState)
  .handleAction(actions.notify, (state, action) => {
    return [...state, action.payload]
  })
  .handleAction(actions.dismiss, (state, action) => {
    return state.filter((noti) => noti.id !== action.payload.id)
  })
