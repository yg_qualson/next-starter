export enum NotificationType {
  INFO,
  WARN,
  ERROR,
}

export interface NotifyPayload {
  type: NotificationType
  id: string
  content: React.ReactNode
}

export interface DismissPayload {
  id: string
}
