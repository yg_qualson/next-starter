import { createAction } from 'typesafe-actions'

import * as T from './notification.types'

export const notify = createAction('@notification/NOTIFY')<T.NotifyPayload>()
export const dismiss = createAction('@notification/DISMISS')<T.DismissPayload>()

export default {
  notify,
  dismiss,
}
