import { fork } from 'typed-redux-saga'

import exampleSaga from '@store/example/example.sagas'

export default function* rootSaga() {
  yield* fork(exampleSaga)
}
