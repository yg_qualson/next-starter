import example from '@store/example/example.actions'
import modal from '@store/modal/modal.actions'
import notification from '@store/notification/notification.actions'

const rootActions = {
  example,
  modal,
  notification,
}

export default rootActions
