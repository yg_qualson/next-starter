import { takeEvery, all } from 'typed-redux-saga'

import { createAPISaga } from '@store/useStore'

import actions from './example.actions'
import * as API from './example.apis'

const saveValueSaga = createAPISaga(actions.saveValue, API.saveValue)

export default function* exampleSaga() {
  yield* all([takeEvery(actions.saveValue.request, saveValueSaga)])
}
