import { createAction } from 'typesafe-actions'

import { createAsyncAction } from '@store/useStore'

import { ExampleValue } from './example.models'
import * as T from './example.types'

const updateValue = createAction('@example/UPDATE_VALUE')<ExampleValue>()

const saveValue = createAsyncAction(
  '@example/SAVE_VALUE/REQUEST',
  '@example/SAVE_VALUE/SUCCESS',
  '@example/SAVE_VALUE/FAILURE'
)<T.SaveValuePayload, T.SaveValueResponse>()

export default {
  updateValue,
  saveValue,
}
