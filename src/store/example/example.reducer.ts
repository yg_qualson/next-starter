import { ActionType, createReducer } from 'typesafe-actions'

import actions from './example.actions'
import { ExampleValue } from './example.models'

interface State {
  value: ExampleValue
}

const initialState: State = {
  value: { id: 0 },
}

const productsReducer = createReducer(initialState)
  .handleAction(actions.updateValue, (state, action) => {
    return {
      ...state,
      value: action.payload,
    }
  })
  .handleAction(actions.saveValue.success, (state, action) => {
    return {
      ...state,
      value: action.payload.response,
    }
  })

export default productsReducer
