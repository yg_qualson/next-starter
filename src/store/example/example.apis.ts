import { API } from '@store/ajax'

import * as T from './example.types'

export const saveValue = (payload: T.SaveValuePayload) => API.post<T.SaveValueResponse>('/save', payload)
