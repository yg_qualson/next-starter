import { ExampleValue } from './example.models'

export interface SaveValuePayload {
  id: number
}

export type SaveValueResponse = ExampleValue
