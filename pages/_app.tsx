import withReduxSaga from 'next-redux-saga'
import { AppProps } from 'next/app'
import { compose } from 'redux'

import { wrapper } from '@store/store'

import { withResponsive } from '@hoc/withResponsive'

import ModalContainer from '@modals/Container'

import NotificationContainer from '@components/Notification'

import { BaseStyle, ResetStyle } from '@styles/index'

function App({ Component, pageProps }: AppProps) {
  return (
    <div id="root">
      <ResetStyle />
      <BaseStyle />
      <Component {...pageProps} />
      <ModalContainer />
      <NotificationContainer />
    </div>
  )
}

const withGlobalSettings = compose(withReduxSaga, wrapper.withRedux, withResponsive)
export default withGlobalSettings(App)
