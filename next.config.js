const package = require('./package.json')

function getAPIEndpoint(deployType) {
  return 'https://example.com'
}

module.exports = {
  images: {
    domains: ['public.realclass.co.kr'],
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    VERSION: package.version,
    API_ENDPOINT: getAPIEndpoint(process.env.DEPLOY_TYPE),
  },
}
